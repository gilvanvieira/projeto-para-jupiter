#include <stdlib.h>
#include <stdio.h>

int funcao(int *pedido, int divisao) {
	int valor = (*pedido) / divisao;
	(*pedido) = (*pedido) %  divisao;
	return valor;
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		return 1;
	}
	int pedido = atoi(argv[1]);
	int hidrogenio = 0, helio = 0, gravidade = 0, gas = 0, vinte = 0, areia = 0;

	gas = funcao(&pedido, 50); 
	vinte = funcao(&pedido, 20);
	gravidade = funcao(&pedido, 10);
	helio = funcao(&pedido, 5);
	areia = funcao(&pedido, 2);
	hidrogenio = pedido;

	printf("%d hidrogenio\n", hidrogenio);
	printf("%d areia\n", areia);
	printf("%d helio\n", helio);
	printf("%d gravidade\n", gravidade);
	printf("%d vintem\n", vinte);
	printf("%d gas\n", gas);

	return 0;
}
