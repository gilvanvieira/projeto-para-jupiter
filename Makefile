CC=gcc
CFLAGS=-Wall -pedantic

atm: atm.c
	${CC} ${CFLAGS} atm.c -o atm
clean:
	rm atm
